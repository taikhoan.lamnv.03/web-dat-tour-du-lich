{ /*jQuery.browser.mobile*/
    /**
     * jQuery.browser.mobile (http://detectmobilebrowser.com/)
     *
     * jQuery.browser.mobile will be true if the browser is a mobile device
     *
     **/
    (function(a) {
        (jQuery.browser = jQuery.browser || {}).mobile = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))
    })(navigator.userAgent || navigator.vendor || window.opera);
}

(function($) {
    $(document).ready(function() {
        (function run() {
            var books = $(".real3dflipbook");
            if (books.length == 0) {
                setTimeout(function() {
                    run();
                }, 1000);
            } else {

                $.each(books, function() {
                    // console.log(this)

                    var id = $(this).attr('id')
                    

                    // var options = window['real3dflipbook_' + id]

                    // var json_str = options.replace(/&quot;/g, '"');
                    // json_str = json_str.replace(/“/g, '"');
                    // json_str = json_str.replace(/”/g, '"');
                    // json_str = json_str.replace(/″/g, '"');
                    // json_str = json_str.replace(/„/g, '"');

                    // json_str = json_str.replace(/«&nbsp;/g, '"');
                    // json_str = json_str.replace(/&nbsp;»/g, '"');


                    // options = jQuery.parseJSON(json_str);
                    var options = $(this).data('flipbook-options')
                    if(typeof options == 'undefined'){

                        options = window['real3dflipbook_' + id]

                        var json_str = options.replace(/&quot;/g, '"');
                        json_str = json_str.replace(/“/g, '"');
                        json_str = json_str.replace(/”/g, '"');
                        json_str = json_str.replace(/″/g, '"');
                        json_str = json_str.replace(/„/g, '"');

                        json_str = json_str.replace(/«&nbsp;/g, '"');
                        json_str = json_str.replace(/&nbsp;»/g, '"');


                        options = jQuery.parseJSON(json_str);

                    }
                    // var options = this.getAttribute('data-flipbook-options')
                    this.removeAttribute('data-flipbook-options');

                    options.assets = {
                        preloader: options.rootFolder + "images/preloader.jpg",
                        left: options.rootFolder + "images/left.png",
                        overlay: options.rootFolder + "images/overlay.jpg",
                        flipMp3: options.rootFolder + "mp3/turnPage.mp3",
                        shadowPng: options.rootFolder + "images/shadow.png",
                        spinner:options.rootFolder + "images/spinner.gif"
                    };

                    options.pdfjsworkerSrc = options.rootFolder + 'js/pdf.worker.min.js'

                    function convertStrings(obj) {

                        $.each(obj, function(key, value) {
                            // console.log(key + ": " + options[key]);
                            if (typeof(value) == 'object' || typeof(value) == 'array') {
                                convertStrings(value)
                            } else if (!isNaN(value)) {
                                if (obj[key] === "")
                                    delete obj[key]
                                else
                                    obj[key] = Number(value)
                            } else if (value == "true") {
                                obj[key] = true
                            } else if (value == "false") {
                                obj[key] = false
                            }
                        });

                    }
                    convertStrings(options)
                    //return

                    if(options.pages){

                        for (var key in options.pages) {
                            options.pages[key].htmlContent = unescape(options.pages[key].htmlContent)
                        }

                        // for (var i = 0; i < options.pages.length; i++) {
                        //     if (typeof(options.pages[i].htmlContent) != 'undefined' && options.pages[i].htmlContent != "" && options.pages[i].htmlContent != "undefined")
                        //         options.pages[i].htmlContent = unescape(options.pages[i].htmlContent)
                        //     else
                        //         delete options.pages[i].htmlContent
                        // }


                    }
                    

                    options.social = [];

                    if (options.facebook == "") delete options.facebook
                    if (options.twitter == "") delete options.twitter
                    if (options.google_plus == "") delete options.google_plus
                    if (options.pinterest == "") delete options.pinterest
                    if (options.email == "") delete options.email
                    if (options.pageWidth == "") delete options.pageWidth
                    if (options.pageHeight == "") delete options.pageHeight

                    if (typeof(options.btnShare) == 'undefined' || !options.btnShare) options.btnShare = { enabled: false }
                    if (typeof(options.btnNext) == 'undefined' || !options.btnNext) options.btnNext = { enabled: false }
                    if (typeof(options.btnPrev) == 'undefined' || !options.btnPrev) options.btnPrev = { enabled: false }
                    if (typeof(options.btnZoomIn) == 'undefined' || !options.btnZoomIn) options.btnZoomIn = { enabled: false }
                    if (typeof(options.btnZoomOut) == 'undefined' || !options.btnZoomOut) options.btnZoomOut = { enabled: false }
                    if (typeof(options.btnToc) == 'undefined' || !options.btnToc) options.btnToc = { enabled: false }
                    if (typeof(options.btnThumbs) == 'undefined' || !options.btnThumbs) options.btnThumbs = { enabled: false }
                    if (typeof(options.btnDownloadPages) == 'undefined' || !options.btnDownloadPages) options.btnDownloadPages = { enabled: false }
                    if (typeof(options.btnDownloadPdf) == 'undefined' || !options.btnDownloadPdf) options.btnDownloadPdf = { enabled: false }
                    if (typeof(options.btnExpand) == 'undefined' || !options.btnExpand) options.btnExpand = { enabled: false }
                    if (typeof(options.btnExpandLightbox) == 'undefined' || !options.btnExpandLightbox) options.btnExpandLightbox = { enabled: false }
                    if (typeof(options.btnSound) == 'undefined' || !options.btnSound) options.btnSound = { enabled: false }
                    if (typeof(options.btnAutoplay) == 'undefined' || !options.btnAutoplay) options.btnAutoplay = { enabled: false }


                    // options.btnShare.enabled = (options.socialShare && options.socialShare.length > 0)
                    if (typeof(options.btnShare.icon) == 'undefined' || options.btnShare.icon == '') options.btnShare.icon = "fa-share";
                    if (typeof(options.btnShare.title) == 'undefined' || options.btnShare.title == '') options.btnShare.title = "Share";

                    if (typeof(options.btnNext.icon) == 'undefined' || options.btnNext.icon == '') options.btnNext.icon = "fa-chevron-right";
                    if (typeof(options.btnNext.title) == 'undefined' || options.btnNext.title == '') options.btnNext.title = "Next page";

                    if (typeof(options.btnPrev.icon) == 'undefined' || options.btnPrev.icon == '') options.btnPrev.icon = "fa-chevron-left";
                    if (typeof(options.btnPrev.title) == 'undefined' || options.btnPrev.title == '') options.btnPrev.title = "Previous page";

                    if (typeof(options.btnZoomIn.icon) == 'undefined' || options.btnZoomIn.icon == '') options.btnZoomIn.icon = "fa-plus";
                    if (typeof(options.btnZoomIn.title) == 'undefined' || options.btnZoomIn.title == '') options.btnZoomIn.title = "Zoom in";

                    if (typeof(options.btnZoomOut.icon) == 'undefined' || options.btnZoomOut.icon == '') options.btnZoomOut.icon = "fa-minus";
                    if (typeof(options.btnZoomOut.title) == 'undefined' || options.btnZoomOut.title == '') options.btnZoomOut.title = "Zoom out";

                    if (typeof(options.btnToc.icon) == 'undefined' || options.btnToc.icon == '') options.btnToc.icon = "fa-list-ol";
                    if (typeof(options.btnToc.title) == 'undefined' || options.btnToc.title == '') options.btnToc.title = "Table of Contents";

                    if (typeof(options.btnThumbs.icon) == 'undefined' || options.btnThumbs.icon == '') options.btnThumbs.icon = "fa-th-large";
                    if (typeof(options.btnThumbs.title) == 'undefined' || options.btnThumbs.title == '') options.btnThumbs.title = "Pages";

                    if (typeof(options.btnDownloadPages.icon) == 'undefined' || options.btnDownloadPages.icon == '') options.btnDownloadPages.icon = "fa-download";
                    if (typeof(options.btnDownloadPages.title) == 'undefined' || options.btnDownloadPages.title == '') options.btnDownloadPages.title = "Download pages";
                    // if(options.downloadPagesUrl)
                    // options.btnDownloadPages.url = options.downloadPagesUrl;

                    if (typeof(options.btnDownloadPdf.icon) == 'undefined' || options.btnDownloadPdf.icon == '') options.btnDownloadPdf.icon = "fa-file";
                    if (typeof(options.btnDownloadPdf.title) == 'undefined' || options.btnDownloadPdf.title == '') options.btnDownloadPdf.title = "Download PDF";
                    // if(options.downloadPdfUrl)
                    // options.btnDownloadPdf.url = options.downloadPdfUrl;

                    if (typeof(options.btnExpand.icon) == 'undefined' || options.btnExpand.icon == '') options.btnExpand.icon = "fa-expand";
                    if (typeof(options.btnExpand.iconAlt) == 'undefined' || options.btnExpand.iconAlt == '') options.btnExpand.iconAlt = "fa-compress";
                    if (typeof(options.btnExpand.title) == 'undefined' || options.btnExpand.title == '') options.btnExpand.title = "Toggle fullscreen";

                    if (typeof(options.btnExpandLightbox.icon) == 'undefined' || options.btnExpandLightbox.icon == '') options.btnExpandLightbox.icon = "fa-expand";
                    if (typeof(options.btnExpandLightbox.iconAlt) == 'undefined' || options.btnExpandLightbox.iconAlt == '') options.btnExpandLightbox.iconAlt = "fa-compress";
                    if (typeof(options.btnExpandLightbox.title) == 'undefined' || options.btnExpandLightbox.title == '') options.btnExpandLightbox.title = "Toggle fullscreen";

                    if (typeof(options.btnSound.icon) == 'undefined' || options.btnSound.icon == '') options.btnSound.icon = "fa-volume-up";
                    if (typeof(options.btnSound.iconAlt) == 'undefined' || options.btnSound.iconAlt == '') options.btnSound.iconAlt = "fa-volume-off";
                    if (typeof(options.btnSound.title) == 'undefined' || options.btnSound.title == '') options.btnSound.title = "Sound";


                    if (typeof(options.btnAutoplay.icon) == 'undefined' || options.btnAutoplay.icon == '') options.btnAutoplay.icon = "fa-play";
                    if (typeof(options.btnAutoplay.iconAlt) == 'undefined' || options.btnAutoplay.iconAlt == '') options.btnAutoplay.iconAlt = "fa-pause";
                    if (typeof(options.btnAutoplay.title) == 'undefined' || options.btnAutoplay.title == '') options.btnAutoplay.title = "Autoplay";

                    if (typeof(options.viewMode) == 'undefined')
                        options.viewMode = "webgl"

                    if (options.btnDownloadPages.url) {
                        options.btnDownloadPages.url = options.btnDownloadPages.url.replace(/\\/g, '/')
                    } else
                        options.btnDownloadPages.enabled = false

                    if(options.btnDownloadPdfUrl){
                        options.btnDownloadPdf.url = options.btnDownloadPdfUrl.replace(/\\/g, '/')
                    }else if (options.btnDownloadPdf.url) {
                        options.btnDownloadPdf.url = options.btnDownloadPdf.url.replace(/\\/g, '/')
                    } else if (options.pdfUrl) {
                        options.btnDownloadPdf.url = options.pdfUrl.replace(/\\/g, '/')
                    }else
                        options.btnDownloadPdf.enabled = false

                    var bookContainer = $(this);

                    switch (options.mode) {

                        case "normal":

                            var containerClass = bookContainer.attr("class")
                            var containerId = bookContainer.attr("id")

                            bookContainer.removeClass(containerClass).addClass(containerClass + "-" + containerId)
                            options.lightBox = false;
                            bookContainer
                                .css("position", "relative")
                                .css("display", "block")
                                .css("height", String(options.height) + "px")
                            // .css("z-index",'999999 !important')
                            bookContainer.flipBook(options);

                            if (options.fitToParent) {

                                window.onresize = function(event) {
                                    fitToParent()
                                };

                                function fitToParent() {
                                    //find parent that has width & height != 0
                                    var parent = findParent(bookContainer);

                                    bookContainer.css("width", String(parent.width()) + "px")
                                    bookContainer.css("height", String(parent.height()) + "px")

                                    function findParent(elem) {
                                        if (elem.parent().width() > 0 && elem.parent().height() > 0)
                                            return elem.parent()
                                        else
                                            return findParent(elem.parent())
                                    }
                                }
                                fitToParent();

                            } else  {

                                jQuery(window).resize(function() {
                                    resizeHeight()
                                });

                                var isAndroid = (/android/gi).test(navigator.appVersion)
                                var isIDevice = (/iphone|ipad/gi).test(navigator.appVersion)

                                var isMobile = jQuery.browser.mobile || isIDevice || isAndroid

                                options.aspectRatio = isMobile && options.aspectRatioMobile ? options.aspectRatioMobile : options.aspectRatio

                                function resizeHeight() {
                                    if (bookContainer.width() > 0)
                                        bookContainer.css("height", String(bookContainer.width() / options.aspectRatio) + "px")
                                }
                                resizeHeight();
                            }
                            break;

                        case "lightbox":

                            bookContainer
                                .css("display", "inline")
                            options.lightBox = true;

                            var containerClass = "real3dflipbook-" + bookContainer.attr("id")


                            var holder = $("<div class='" + containerClass + "'>")
                            holder.attr('style', options.lightboxContainerCSS)
                            bookContainer.before(holder)
                            bookContainer.remove();

                            if (options.lightboxThumbnailUrl && options.lightboxThumbnailUrl != "") {

                                var thumb = $('<img></img>').attr('src', options.lightboxThumbnailUrl).appendTo(holder)
                                thumb.attr('style', options.lightboxThumbnailUrlCSS)

                            }

                            if (options.lightboxText && options.lightboxText != "") {
                                var link = $('<a/>').text(options.lightboxText)
                                if (options.lightboxTextPosition == 'top')
                                    link.prependTo(holder)
                                else
                                    link.appendTo(holder)
                                link.attr('style', options.lightboxTextCSS)
                            }

                            // var book = holder.flipBook(options);

                            // bookContainer.flipBook(options)

                            if(!options.lightboxCssClass || options.lightboxCssClass == "")
                                options.lightboxCssClass = containerClass

                            holder.addClass(options.lightboxCssClass)

                            $("." + options.lightboxCssClass).flipBook(options);


                            //   options.lightBox = true;

                            // if (options.lightboxCssClass && options.lightboxCssClass != "") {
                            //     bookContainer.addClass(options.lightboxCssClass)
                            //     $("." + options.lightboxCssClass).flipBook(options);
                            // }else{
                            //     bookContainer.flipBook(options)
                            // }

                            break;

                        case "fullscreen":

                            options.lightBox = false;
                            var elem = 'body'

                            bookContainer
                                .appendTo(elem)
                                .css("position", "fixed")
                                .css("top","0")
                                .css("bottom", "0")
                                .css("left", "0")
                                .css("right", "0")
                                // .css('top', options.offsetTop + 'px')
                                //flipbook on top of everything - over the site menu
                                // .css('z-index', options.zIndex || '2147483647');
                                .css('z-index', '2147483647');
                            bookContainer.flipBook(options);
                            $('body').css('overflow', 'hidden');

                            if (options.menuSelector) {

                                var $menu = $(options.menuSelector)
                                bookContainer.css('top', $menu.height() + 'px')
                                window.onresize = function(event) {
                                    bookContainer.css('top', $menu.height() + 'px')
                                };

                            }
                            break;
                    }

                })
            }
        })();
    });
}(jQuery));