$(document).ready(function () {
  var dtTable = $('#ContactTable').dataTable();
  dtTable.fnDestroy();
  dtTable = $('#ContactTable').dataTable({
    "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100,"Tất cả"]],
    "language": {
      "sProcessing": "Đang xử lý...",
      "sLengthMenu": "Xem _MENU_ kết quả",
      "sZeroRecords": "Không tìm thấy dòng nào phù hợp",
      "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ kết quả",
      "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 kết quả",
      "sInfoFiltered": "(được lọc từ _MAX_ kết quả)",
      "sInfoPostFix": "",
      "sSearch": "Tìm:",
      "sUrl": "",
      "oPaginate": {
        "sFirst": "Đầu",
        "sPrevious": "Trước",
        "sNext": "Tiếp",
        "sLast": "Cuối"
      }
    }
  });
});
