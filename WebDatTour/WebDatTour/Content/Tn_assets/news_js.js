﻿$(document).ready(function () {
	if (window.matchMedia("(max-width: 768px)").matches) {
		/* the viewport is less than 768 pixels wide */
		$('#recentlyNews').slick({
			dots: true,
			infinite: true,
			speed: 500,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2000
		});
	}
	else {
		$('#recentlyNews').slick({
			dots: true,
			infinite: true,
			speed: 500,
			slidesToShow: 4,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2000
		});
	}
});
