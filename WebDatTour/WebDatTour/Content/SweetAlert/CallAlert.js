﻿var value = undefined;

function getValue(result) {
    value = result;
}


function callAlert(Title, Message, Flag = "success", ConfirmButtonText = "Ok", ConfirmCancelText = undefined)
{
    var showCancel = (ConfirmCancelText !== undefined && ConfirmCancelText !== null && ConfirmCancelText !== "") ? (true) : (false);
    data = swal({
        title: Title,
        type: Flag,
        html: Message,
        showCloseButton: true,
        showCancelButton: showCancel,
        focusConfirm: false,
        confirmButtonText: ConfirmButtonText,
        cancelButtonText: ConfirmCancelText
    });
    return data;
}