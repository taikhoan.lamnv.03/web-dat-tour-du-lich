﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebDatTour.Models;

namespace WebDatTour.Controllers
{
    public class DiaDiemsController : Controller
    {
        private ModelDatTour db = new ModelDatTour();

        // GET: DiaDiems
        public ActionResult Index()
        {
            var diaDiems = db.DiaDiems.Include(d => d.TinhThanh);
            return View(diaDiems.ToList());
        }
        public ActionResult getNameTinhThanhByIdQuocGia(int maQuocGia)
        {
            try
            {
                QuocGia[] lstQuocGia = db.QuocGias.ToArray();
                TinhThanh[] lstTinhThanh = db.TinhThanhs.ToArray();
                List<object> lstResult = new List<object>();
                foreach(var item in lstTinhThanh)
                {
                    lstResult.Add(new
                    {
                        tinhthanh = ReflectionHelper.ModelToObject(item),
                        tenQuocGia = lstQuocGia.Where(x => x.maQuocGia == item.maQuocGia).FirstOrDefault(),

                    });
                }
                return Json(new { success = true, contents = ReflectionHelper.ArrayModelToArrObject(lstResult.ToArray()) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, contents = ex }, JsonRequestBehavior.AllowGet);
            }
            
        }
        // GET: DiaDiems/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DiaDiem diaDiem = db.DiaDiems.Find(id);
            if (diaDiem == null)
            {
                return HttpNotFound();
            }
            return View(diaDiem);
        }

        // GET: DiaDiems/Create
        public ActionResult Create()
        {
            ViewBag.maTinhThanh = new SelectList(db.TinhThanhs, "maTinhThanh", "tenTinhThanh");
            return View();
        }

        // POST: DiaDiems/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "maDiaDiem,tenDiaDiem,moTaDiaDiem,hinhAnhDiaDiem,maTinhThanh")] DiaDiem diaDiem)
        {
            if (ModelState.IsValid)
            {
                db.DiaDiems.Add(diaDiem);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.maTinhThanh = new SelectList(db.TinhThanhs, "maTinhThanh", "tenTinhThanh", diaDiem.maTinhThanh);
            return View(diaDiem);
        }

        // GET: DiaDiems/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DiaDiem diaDiem = db.DiaDiems.Find(id);
            if (diaDiem == null)
            {
                return HttpNotFound();
            }
            ViewBag.maTinhThanh = new SelectList(db.TinhThanhs, "maTinhThanh", "tenTinhThanh", diaDiem.maTinhThanh);
            return View(diaDiem);
        }

        // POST: DiaDiems/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "maDiaDiem,tenDiaDiem,moTaDiaDiem,hinhAnhDiaDiem,maTinhThanh")] DiaDiem diaDiem)
        {
            if (ModelState.IsValid)
            {
                db.Entry(diaDiem).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.maTinhThanh = new SelectList(db.TinhThanhs, "maTinhThanh", "tenTinhThanh", diaDiem.maTinhThanh);
            return View(diaDiem);
        }

        // GET: DiaDiems/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DiaDiem diaDiem = db.DiaDiems.Find(id);
            if (diaDiem == null)
            {
                return HttpNotFound();
            }
            return View(diaDiem);
        }

        // POST: DiaDiems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DiaDiem diaDiem = db.DiaDiems.Find(id);
            db.DiaDiems.Remove(diaDiem);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult getAllDiaDiemByIdTinhThanh(int maTinhThanh)
        {
            return Json(new { success = true, contents = ReflectionHelper.ArrayModelToArrObject(db.DiaDiems.Where(x => x.maTinhThanh == maTinhThanh).ToArray()) }, JsonRequestBehavior.AllowGet);
        }
    }
}
