﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebDatTour.Models;

namespace WebDatTour.Controllers
{
    public class TourController : Controller
    {
        private ModelDatTour db = new ModelDatTour();

        // GET: Tour
        public ActionResult Tim()
        {
            return View();
        }

        //GET: Xem
        public ActionResult Xem()
        {
            return View();
        }
        //GET: Dat
        public ActionResult Dat()
        {
            return View();
        }

        // GET ALL TOUR
        public ActionResult GetAllTour()
        {
            try
            {
                return Json(new { success = true, contents = ReflectionHelper.ListModelToLstObject(db.ThongTinTours.ToList()) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, contents = ex }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}