namespace WebDatTour.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ThongTinCaNhan")]
    public partial class ThongTinCaNhan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ThongTinCaNhan()
        {
            BinhLuans = new HashSet<BinhLuan>();
            ThongTinPhieuDatTours = new HashSet<ThongTinPhieuDatTour>();
        }

        [Key]
        public int maThongTinCaNhan { get; set; }

        public string hoTen { get; set; }

        public string email { get; set; }

        public string sdt { get; set; }

        public string cmnd { get; set; }

        [StringLength(10)]
        public string gioiTinh { get; set; }

        public string diaChi { get; set; }

        public string taiKhoan { get; set; }

        public string matKhau { get; set; }

        public string loaiTaiKhoan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BinhLuan> BinhLuans { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ThongTinPhieuDatTour> ThongTinPhieuDatTours { get; set; }
    }
}
