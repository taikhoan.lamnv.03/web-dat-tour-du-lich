namespace WebDatTour.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ThongTinPhieuDatTour")]
    public partial class ThongTinPhieuDatTour
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ThongTinPhieuDatTour()
        {
            ThongTinNguoiDiTours = new HashSet<ThongTinNguoiDiTour>();
        }

        [Key]
        public int maPhieuDatTour { get; set; }

        public int? maThongTinCaNhan { get; set; }

        public int? maTour { get; set; }

        public int? soNguoiDi { get; set; }

        public string trangThai { get; set; }

        [Column(TypeName = "money")]
        public decimal? tongTien { get; set; }

        public virtual ThongTinCaNhan ThongTinCaNhan { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ThongTinNguoiDiTour> ThongTinNguoiDiTours { get; set; }

        public virtual ThongTinTour ThongTinTour { get; set; }
    }
}
