namespace WebDatTour.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ThongTinNguoiDiTour")]
    public partial class ThongTinNguoiDiTour
    {
        [Key]
        public int maThongTinNguoiDiTour { get; set; }

        public int? maPhieuDatTour { get; set; }

        public string hoTen { get; set; }

        [StringLength(10)]
        public string gioiTinh { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ngaySinh { get; set; }

        public string doTuoi { get; set; }

        public virtual ThongTinPhieuDatTour ThongTinPhieuDatTour { get; set; }
    }
}
