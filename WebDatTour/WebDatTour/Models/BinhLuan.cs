namespace WebDatTour.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BinhLuan")]
    public partial class BinhLuan
    {
        [Key]
        public int maBinhLuan { get; set; }

        public int? maThongTinCaNhan { get; set; }

        public int? maTour { get; set; }

        public string noiDungBinhLuan { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ngayBinhLuan { get; set; }

        public virtual ThongTinCaNhan ThongTinCaNhan { get; set; }

        public virtual ThongTinTour ThongTinTour { get; set; }
    }
}
