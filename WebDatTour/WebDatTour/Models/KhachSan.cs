namespace WebDatTour.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("KhachSan")]
    public partial class KhachSan
    {
        [Key]
        public int maKhachSan { get; set; }

        public string tenKhachSan { get; set; }

        public string nguoiDaiDien { get; set; }

        public string diaChi { get; set; }

        public int? maDiaDiem { get; set; }

        public virtual DiaDiem DiaDiem { get; set; }
    }
}
