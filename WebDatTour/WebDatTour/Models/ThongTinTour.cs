namespace WebDatTour.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ThongTinTour")]
    public partial class ThongTinTour
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ThongTinTour()
        {
            BinhLuans = new HashSet<BinhLuan>();
            ThongTinPhieuDatTours = new HashSet<ThongTinPhieuDatTour>();
        }

        [Key]
        public int maTour { get; set; }

        public string tenTour { get; set; }

        public string hinhAnh { get; set; }

        public string noiDungTour { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ngayDi { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ngayVe { get; set; }

        public double? giaTour { get; set; }

        public string tinhTrangTour { get; set; }

        public int? maDiaDiem { get; set; }

        public int? maLichTrinh { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BinhLuan> BinhLuans { get; set; }

        public virtual DiaDiem DiaDiem { get; set; }

        public virtual LichTrinh LichTrinh { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ThongTinPhieuDatTour> ThongTinPhieuDatTours { get; set; }
    }
}
