namespace WebDatTour.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LichTrinh")]
    public partial class LichTrinh
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public LichTrinh()
        {
            ThongTinTours = new HashSet<ThongTinTour>();
        }

        [Key]
        public int maLichTrinh { get; set; }

        public string tenLichTrinh { get; set; }

        public string tieuDeCuaNgay { get; set; }

        public string noiDungCuaNgay { get; set; }

        public string ghiChu { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ThongTinTour> ThongTinTours { get; set; }
    }
}
