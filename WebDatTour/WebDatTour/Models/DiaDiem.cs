namespace WebDatTour.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DiaDiem")]
    public partial class DiaDiem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DiaDiem()
        {
            KhachSans = new HashSet<KhachSan>();
            ThongTinTours = new HashSet<ThongTinTour>();
        }

        [Key]
        public int maDiaDiem { get; set; }

        public string tenDiaDiem { get; set; }

        public string moTaDiaDiem { get; set; }

        public string hinhAnhDiaDiem { get; set; }

        public int? maTinhThanh { get; set; }

        public virtual TinhThanh TinhThanh { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KhachSan> KhachSans { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ThongTinTour> ThongTinTours { get; set; }
    }
}
