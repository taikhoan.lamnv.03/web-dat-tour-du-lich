namespace WebDatTour.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ModelDatTour : DbContext
    {
        public ModelDatTour()
            : base("name=ModelDatTour2")
        {
        }

        public virtual DbSet<BinhLuan> BinhLuans { get; set; }
        public virtual DbSet<DiaDiem> DiaDiems { get; set; }
        public virtual DbSet<KhachSan> KhachSans { get; set; }
        public virtual DbSet<LichTrinh> LichTrinhs { get; set; }
        public virtual DbSet<QuocGia> QuocGias { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<ThongTinCaNhan> ThongTinCaNhans { get; set; }
        public virtual DbSet<ThongTinNguoiDiTour> ThongTinNguoiDiTours { get; set; }
        public virtual DbSet<ThongTinPhieuDatTour> ThongTinPhieuDatTours { get; set; }
        public virtual DbSet<ThongTinTour> ThongTinTours { get; set; }
        public virtual DbSet<TinhThanh> TinhThanhs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ThongTinPhieuDatTour>()
                .Property(e => e.tongTien)
                .HasPrecision(19, 4);
        }
    }
}
